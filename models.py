from sqlalchemy import Column, Integer, String

from database import Base


class Recipe(Base):
    __tablename__ = "Recipe"
    id = Column(Integer, primary_key=True, index=True)
    title = Column(String, index=True)
    views = Column(Integer, index=True)
    cooking_time = Column(Integer, index=True)
    ingredients = Column(String, index=True)
    instructions = Column(String, index=True)
