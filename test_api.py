from fastapi.testclient import TestClient

from main import app

client = TestClient(app)


def test_add_recipe():
    recipe_data = {
        "title": "Test recipe",
        "views": 0,
        "cooking_time": 10,
        "ingredients": "a, b, c, d",
        "instructions": "let him cook",
    }

    response = client.post("/add_recipe/", json=recipe_data)

    assert response.status_code == 200
    assert "id" in response.json().keys()
    assert response.json().get("title") == recipe_data.get("title")


def test_get_recipes():
    response = client.get("/recipes/")
    assert response.status_code == 200

    recipes = response.json()
    assert recipes[0]["title"] == "New recipe"
    assert recipes[1]["title"] == "Some recipe"


def test_get_recipe():
    recipe_id = 1
    response = client.get(f"/recipe/{recipe_id}")
    assert response.status_code == 200

    recipe = response.json()
    assert recipe["title"] == "New recipe"
