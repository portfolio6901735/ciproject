from typing import List, Optional

from fastapi import FastAPI
from sqlalchemy import select

from database import engine, session
from schemas import RecipeOut, RecipeIn, RecipeList
from models import Base, Recipe

app = FastAPI()


@app.on_event("startup")
async def startup():
    async with engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


@app.on_event("shutdown")
async def shutdown():
    await engine.dispose()
    await session.close()


@app.post("/add_recipe/", response_model=RecipeOut)
async def add_recipe(recipe: RecipeIn) -> Recipe:
    new_recipe = Recipe(**recipe.dict())
    async with session.begin():
        session.add(new_recipe)
    return new_recipe


@app.get("/recipes/", response_model=List[RecipeList])
async def get_recipes() -> List[Recipe]:
    async with session.begin():
        statement = select(Recipe).order_by(
            Recipe.views.desc(), Recipe.cooking_time.asc()
        )
        result = await session.execute(statement)
        return result.scalars().all()


@app.get("/recipe/{recipe_id}", response_model=RecipeOut)
async def get_recipe(recipe_id: Optional[int] = None) -> Recipe:
    async with session.begin():
        statement = select(Recipe).filter(Recipe.id == recipe_id)
        result = await session.execute(statement)
        recipe = result.scalar_one()
        recipe.views += 1
        return recipe
