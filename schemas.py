from pydantic import BaseModel


class BaseRecipe(BaseModel):
    title: str
    views: int
    cooking_time: int
    ingredients: str
    instructions: str


class RecipeList(BaseModel):
    id: int
    title: str
    views: int
    cooking_time: int


class RecipeIn(BaseRecipe):
    pass


class RecipeOut(BaseRecipe):
    id: int

    class Config:
        orm_mode = True
